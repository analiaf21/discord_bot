class DiscordBot 
    def initialize 
        config = File.foreach('config.txt').map { |line| line.split(' ').join(' ')}
        @token = config[0].to_s
        @client_id = config[1].to_s
        @webhooks_url = config[2].to_s

        @my_bot = Discordrb::Bot.new token: @token, client_id: @client_id
        @my_bot.run true
    end

    # Busca los mensajes del canal para ver quienes estuvieron presentes
    # y envía la lista al canal #general
    def send_presentes(channel_name)
        channel = @my_bot.find_channel(channel_name)
        messages = channel[0].history(100)
        lista = messages.join(", ")
        
        client = Discordrb::Webhooks::Client.new(url: @webhooks_url)
        client.execute do |builder|
            builder.content = 'Lista de presentes'
            builder.add_embed do |embed|
            embed.title = "Fecha #{channel_name}"
            embed.description = lista
            embed.timestamp = Time.now
            end
        end
    end
end