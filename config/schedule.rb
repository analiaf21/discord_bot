require_relative '../rakefile.rb'

# Lista de tareas 

# Todos los dìas a la misma hora se manda la lista de presentes al canal #general
every 1.day, :at => '10:00 am' do
    rake 'alumnos:listar_presentes'
end

# Para probar
every 1.minute do
    rake 'alumnos:listar_presentes'
end
