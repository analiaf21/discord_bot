require "rake"
require "logger"
require "discordrb"
require 'discordrb/webhooks'
require "json"
require_relative 'discord_bot.rb'
require 'time'

# Tarea que llama al bot para enviar la lista de presentes
namespace :alumnos do
  task :listar_presentes do
      # El nombre del canal tiene el formato: 'YYYYmmdd' y se toma el día de ayer
      channel_name = (Time.new - 86400).strftime "%Y%m%d"
      @bot = DiscordBot.new
      @bot.send_presentes(channel_name)
    end
  end